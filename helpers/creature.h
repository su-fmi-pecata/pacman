#ifndef CREATURE_H_INCLUDED
#define CREATURE_H_INCLUDED

#define SQUARE_SIZE_DEFINED 20.f
#define CIRCLE_SIZE_DEFINED 5.f
#define MAX_PATH_LENGTH_DEFINED 100

#include <SFML/Graphics.hpp>
#include <time.h>
#include <random>

struct Point {
	unsigned short x = 0;
	unsigned short y = 0;
};

struct Creature : Point {
	char previous = ' ';
	char teleport = 0;
	unsigned short currentIteration = 0;

	Point starting;

	sf::RectangleShape shape = sf::RectangleShape(sf::Vector2f(SQUARE_SIZE_DEFINED, SQUARE_SIZE_DEFINED));
};

struct Pacman : Creature {
	unsigned short points = 0;
	unsigned short remainingLifes = 3;
};

struct Ghost : Creature {
	unsigned short id = 0;
	unsigned short lastDirection = 0;
	unsigned short maxIterations = rand()%13+7;
	bool followPath = false;

	void checkForRandom(void) {
		currentIteration = (currentIteration) % maxIterations;
		if (currentIteration == 0) {
			unsigned short c;
			while (lastDirection == (c = rand() % 4));
			lastDirection = c;
		}
	}
};

struct GhostFollow : Ghost {
	unsigned short leftPathIndex = 0;
	unsigned short rightPathIndex = 0;
	const unsigned short MAX_PATH_LENGTH = MAX_PATH_LENGTH_DEFINED;
	Point path[MAX_PATH_LENGTH_DEFINED];
};

#endif // CREATURE_H_INCLUDED
