#include "creature.h"
#include "creaturesClass.h"

Creatures::Creatures(void) {
	pacman.shape.setFillColor(sf::Color::Yellow);
	ghostBlue.shape.setFillColor(sf::Color::Cyan);
	ghostBlue.followPath = true;
	ghostRed.shape.setFillColor(sf::Color::Red);
	ghostGreen.shape.setFillColor(sf::Color::Green);
	ghostYellow.shape.setFillColor(sf::Color::Magenta);
}

Creature& Creatures::getCreature(const char cell) {
	if (cell == 'r') {
		return ghostRed;
	} else if (cell == 'g') {
		return ghostGreen;
	} else if (cell == 'b') {
		return ghostBlue;
	} else if (cell == 'y') {
		return ghostYellow;
	} else if (cell == 'P') {
		return pacman;
	}
}

bool Creatures::isCreature(const char& cell) {
	bool in = false;
	for (int i = 0; i < CreaturesListLength && !in; i++)
		in = in || (CreaturesList[i] == cell);

	return in;
}