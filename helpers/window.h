#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED

#include <SFML/Graphics.hpp>
#include "maze.h"
#include "creaturesClass.h"
/**
	Class managing sfml window.
*/

class GameWindow {
	private:
		static const unsigned short MAX_APP_NAME_LENGTH = 100;

		const unsigned short MAX_SCREEN_WIDTH = 1000;
		const unsigned short MAX_SCREEN_HEIGHT = 700;
		const unsigned short MIN_SCREEN_WIDTH = 100;
		const unsigned short MIN_SCREEN_HEIGHT = 70;
		const char* const APP_HEADER = "PACMAN";

		char appNameArr[MAX_APP_NAME_LENGTH] = { 0 };
		std::unique_ptr<sf::RenderWindow> gameRenderWindow;

		const unsigned short screenWidth;
		const unsigned short screenHeight;
		const char* const appName = appNameArr;
		Creatures& creatures;

		#pragma region objects for drawing
		const float CIRCLE_SIZE = CIRCLE_SIZE_DEFINED;
		const float SQUARE_SIZE = SQUARE_SIZE_DEFINED;
		sf::Vector2f defaultRectangleSize = sf::Vector2f(SQUARE_SIZE, SQUARE_SIZE);

		sf::RectangleShape boxBorder = sf::RectangleShape(defaultRectangleSize);
		sf::RectangleShape boxEmpty  = sf::RectangleShape(defaultRectangleSize);
		sf::RectangleShape fence  = sf::RectangleShape(sf::Vector2f(SQUARE_SIZE, SQUARE_SIZE/4));
		sf::CircleShape food = sf::CircleShape(CIRCLE_SIZE);
		sf::CircleShape foodBig = sf::CircleShape(CIRCLE_SIZE);

		sf::Font arialFont;
		sf::Font robotoFont;
		sf::Font robotoBoldFont;
		sf::Text pointsPrefix;
		sf::Text lifesPrefix;
		sf::Text pointsNumber;
		sf::Text lifesNumber;
		sf::Text loseMessage;
		bool initialisedTextPositions = false;
		#pragma endregion

		void setText(sf::Text&, const char*);
		void _init(const char* const);
		void drawBox(const char&, const int&, const int&);

	public:
		GameWindow(const unsigned short&, const unsigned short&, const char* const, Creatures&);
		void init();
		void close();
		void draw(const Maze&, const unsigned short&, const unsigned short&);
		void printLoseMessage(const char);
		bool hasEventToHandle(sf::Event&);
};

#endif // WINDOW_H_INCLUDED
