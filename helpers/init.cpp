#include "init.h"
#include "helper.h"
#include "rapidjson/document.h"
#include "rapidjson/encodings.h"
#include "config.h"
#include "gameData.h"
#include "window.h"
#include <stdio.h>

Init::Init(void) {}

void Init::readConfig(void) {
	return Init::readConfig("data/config.json");
}

void Init::readConfig(const char* filename) {
	printf("\nreadConfig Path = %s\n", filename);
	configFile = parseFile.readFile(filename);

	setConfigData();
	setGameWindow();
}

void Init::readLevel(void) {
	return Init::readLevel("default");
}

void Init::readLevel(const char* filename) {
	#pragma region generate filename and read file
	char filePath[115] = "data/level-";
	Helper::concatenate(filename, filePath);
	Helper::concatenate(".json", filePath);
	levelFile = parseFile.readFile(filePath);
	#pragma endregion

	const rapidjson::Value& _map = levelFile["map"];
	//gameWindow->printLoseMessage(false);
	gameData = new GameData(_map, _map[0].GetStringLength(), _map.Size(), creatures, *gameWindow);//&((*gameWindow).printLoseMessage));
}

void Init::setConfigData(void) {
	#pragma region read StartLevelSelect
	rapidjson::Value& tmp = configFile["StartLevelSelect"];
	bool _startLevelSelect = false;
	try {
		assert(tmp.IsBool());
		_startLevelSelect = tmp.GetBool();
	} catch (const std::exception&) {}
	#pragma endregion

	#pragma region read LevelName
	tmp = configFile["LevelName"];
	const char* _levelName = "";
	try {
		assert(tmp.IsString());
		_levelName = tmp.GetString();
	} catch (const std::exception&) {}
	#pragma endregion

	config = new Config(_startLevelSelect, _levelName);
}

void Init::setGameWindow(void) {
	rapidjson::Value& gameObject = configFile["Game"];

	#pragma region read WindowName
	rapidjson::Value& tmp = gameObject["WindowName"];
	const char* _appWindowName = "";
	try {
		assert(tmp.IsString());
		_appWindowName = tmp.GetString();
	}
	catch (const std::exception&) {}
	#pragma endregion

	#pragma region read ScreenWidth
	tmp = gameObject["ScreenWidth"];
	unsigned short _windowWidth = 0;
	try {
		assert(tmp.IsUint());
		_windowWidth = tmp.GetUint();
	}
	catch (const std::exception&) {}
	#pragma endregion

	#pragma region read ScreenHeight
	tmp = gameObject["ScreenHeight"];
	unsigned short _windowHeight = 0;
	try {
		assert(tmp.IsUint());
		_windowHeight = tmp.GetUint();
	}
	catch (const std::exception&) {}
	#pragma endregion

	gameWindow = new GameWindow(_windowWidth, _windowHeight, _appWindowName, creatures);
}
