#ifndef MAZE_H_INCLUDED
#define MAZE_H_INCLUDED

#include "creature.h"

constexpr short MAZE_MAX_HEIGTH = 50, MAZE_MAX_WIDTH = 50;

typedef char maze_type[MAZE_MAX_HEIGTH][MAZE_MAX_WIDTH];

class Maze {
	public:
		const maze_type& map;
		const int width;
		const int height;

		Maze(const maze_type& _map, const int _width, const int _height)
			:	map(_map),
				width(_width),
				height(_height)
		{

		}
};

struct Teleport {
	Point a;
	Point b;
	bool init = false;
};

struct QueueNode
{
	Point pt;
	int dist;
};

#endif // MAZE_H_INCLUDED
