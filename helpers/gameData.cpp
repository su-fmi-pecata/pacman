#include "gameData.h"
#include "window.h"
#include "helper.h"
#include "maze.h"
#include "creaturesClass.h"
#include "rapidjson/document.h"
#include <SFML/Graphics.hpp>
#include <queue>
#include <time.h>

#include <stdio.h>

GameData::GameData(const rapidjson::Value& _map, const unsigned short _width, const unsigned short _height, Creatures& _creature, GameWindow& gw)
//void (GameWindow::*_loseLifeMessagePrinter)(char) )
	:	width(_width),
		height(_height),
		maze(map, _width, _height),
		creatures(_creature),
		gameWindow(gw)
		//loseLifeMessagePrinter2(_loseLifeMessagePrinter)
{
	unsigned short creatureID = 0;
	for (rapidjson::SizeType i = 0; i < _map.Size(); i++)
	{
		const rapidjson::Value &data_vec = _map[i];
		Helper::concatenate(_map[i].GetString(), map[i]);
		for (int q = 0;  q < maze.width;  q++) {
			const char& cell = map[i][q];
			if (creatures.isCreature(cell)) { // pacman or ghost
				Creature& creature = creatures.getCreature(cell);
				creature.x = q;
				creature.y = i;
				creature.starting.x = q;
				creature.starting.y = i;
				if (!isPacman(q, i)) {
					((Ghost&)creature).id = creatureID++;
				}
			} else if ('0' <= cell && cell <= '9') { // teleport
				Teleport& t = teleports[cell];
				Point& point = (t.init ? t.b : t.a);
				point.x = q;
				point.y = i;
				t.init = true;
			} else if (cell == '-') {
				Point& p = fences[numberOfFences++];
				p.x = q;
				p.y = i;
			}
		}
	}
}

void GameData::stopGame(void) {
	play = false;
}

void GameData::loseLife(void) {
	justLostLife = true;
	Pacman& p = getPacman();
	p.remainingLifes--;
	char message = '-';
	if (p.points == 240) {
		message = 'w';
	}
	else if (p.remainingLifes == 0) {
		message = 'l';
	}
	gameWindow.printLoseMessage(message);

	for (unsigned short i = creatures.CreaturesListLength - 1; i > 0; i--) {
		const char& id = creatures.CreaturesList[i];
		Ghost& g = (Ghost&) creatures.getCreature(id);

		map[g.y][g.x] = g.previous;
		map[g.starting.y][g.starting.x] = id;

		g.previous = ' ';
		g.x = g.starting.x;
		g.y = g.starting.y;

		if (g.followPath) {
			GhostFollow& gf = (GhostFollow&)g;
			gf.leftPathIndex = gf.rightPathIndex;
		}
	}

	move(p, p.starting.x, p.starting.y, false);
	if (message != '-') {
		stopGame();
	}
}

#pragma region getters
bool GameData::isPlaying(void) {
	return play;
}

const Maze& GameData::getMaze(void) {
	return maze;
}

Pacman& GameData::getPacman(void) {
	return (Pacman&)creatures.getCreature(creatures.CreaturesList[0]);
}
#pragma endregion

#pragma region is cell special
bool GameData::isPacman(const unsigned short& x, const unsigned short& y) {
	return map[y][x] == creatures.CreaturesList[0];
}

bool GameData::isGhost(const unsigned short& x, const unsigned short& y) {
	return creatures.isCreature(map[y][x]) && !isPacman(x, y);
}

bool GameData::isTeleport(const unsigned short& x, const unsigned short& y) {
	return '0' <= map[y][x] && map[y][x] <= '9';
}

bool GameData::isBigFood(const unsigned short& x, const unsigned short& y) {
	return 'O' == map[y][x];
}
#pragma endregion

void GameData::move(Creature& creature, unsigned short& newX, unsigned short& newY, const bool isCreatureGhost) {
	if (creature.x == newX && creature.y == newY) {
		return;
	}

	creature.currentIteration++;
	if ((isCreatureGhost && isPacman(newX, newY)) || (!isCreatureGhost && isGhost(newX, newY))) {
		loseLife();
		return;
	} else {
		//printf("\nMove to free");
	}

	char& oldCell = map[creature.y][creature.x];
	bool onTeleport = isTeleport(newX, newY);
	if (onTeleport) { // if on teleport -> get other gate coordinates
		const Teleport& t = teleports[map[newY][newX]];
		if (t.a.x == newX && t.a.y == newY) {
			newX = t.b.x;
			newY = t.b.y;
		} else {
			newX = t.a.x;
			newY = t.a.y;
		}
	}
	char& newCell = map[newY][newX];

	const char replaceWith = creature.teleport ? creature.teleport : creature.previous;
	creature.teleport = 0;
	const char identificator = oldCell; // char of creature
	//printf("\noldCell[%c] \t newCell[%c] \t replaceWith[%c] \t identificator[%c]", oldCell, newCell, replaceWith, identificator);
	if (isCreatureGhost) {
		creature.previous = newCell;
	}
	if (onTeleport) {
		creature.teleport = newCell;
	}
	newCell = identificator;
	oldCell = replaceWith;
	creature.x = newX;
	creature.y = newY;
}

bool GameData::checkMove(const unsigned short& x, const unsigned short& y) {
	//printf("\nMove ch -> (%02d,%02d)[%c] -> %d_%d", x, y, map[y][x], (map[y][x] != '#'), (map[y][x] != '-'));
	return 0 <= x && x < maze.width // x in map
		&& 0 <= y && y < maze.height // y in map
		&& map[y][x] != '#'  // not wall
		&& map[y][x] != '-' // not gate
		&& map[y][x] != 0; // not terminator
}

void GameData::getNewCoordinatees(unsigned short& newX, unsigned short& newY, const unsigned short& direction) {
	/*
		0 - left (default)
		1 - right
		2 - top
		3 - bottom
	*/
	switch (direction) {
		case 1:
			newX++;
			break;
		case 2:
			newY--;
			break;
		case 3:
			newY++;
			break;
		default:
			newX--;
			break;
	}
}

bool GameData::bfs(const unsigned short& fromX, const unsigned short& fromY, const unsigned short& toX, const unsigned short& toY, Point* points, unsigned short& numberOfPoints) {
	//printf("\nBFS(fromX: %02d, fromY: %02d, toX: %02d, toY: %02d", fromX, fromY, toX, toY);
	std::queue<QueueNode> q;
	QueueNode qNode;
	qNode.pt.x = fromX;
	qNode.pt.y = fromY;
	qNode.dist = 0;
	q.push(qNode);
	bfsArrayPath[fromY][fromX].x = 0;
	bfsArrayPath[fromY][fromX].y = 0;
	memset(bfsArrayVisited, false, sizeof bfsArrayVisited);

	bool found = false;

	while (!found && !q.empty()) {
		qNode = q.front();
		q.pop();
		found = (qNode.pt.x == toX && qNode.pt.y == toY);
		if (found) break;
		bfsArrayVisited[qNode.pt.y][qNode.pt.x] = true;

		unsigned short x;
		unsigned short y;
		for (unsigned short direction = 0; direction < 4; direction++) {
			x = qNode.pt.x;
			y = qNode.pt.y;
			getNewCoordinatees(x, y, direction);
			if (checkMove(x, y) && !bfsArrayVisited[y][x]) {
				bfsArrayVisited[y][x] = true;
				QueueNode newNode;
				newNode.dist = qNode.dist + 1;
				newNode.pt.x = x;
				newNode.pt.y = y;
				q.push(newNode);
				bfsArrayPath[y][x].x = qNode.pt.x;
				bfsArrayPath[y][x].y = qNode.pt.y;

				found = (x == toX && y == toY);
				if (found) {
					qNode = newNode; // set found point to qNode
					break;
				}
			}
		}
	}

	if (found) {
		numberOfPoints = qNode.dist;
		points[numberOfPoints].x = toX;
		points[numberOfPoints].y = toY;
		numberOfPoints++;
		Point p = bfsArrayPath[toY][toX];
		for (int i = numberOfPoints - 2; i >= 0; i--) {
			points[i] = p;
			p = bfsArrayPath[p.y][p.x];
		}
	}

	return found;
}

void GameData::findMoveBots(Ghost& ghost, Point& newPos) {
	ghost.checkForRandom();
	const short mask = 4 - 1;
	unsigned short direction;
	unsigned short& newX = newPos.x;
	unsigned short& newY = newPos.y;
	bool canMove = false;
	for (unsigned short i = 0; i < 4 && !canMove; i++) {
		direction = ghost.lastDirection;
		newX = ghost.x;
		newY = ghost.y;
		if (i == 0) {
			// already set
		} else if (i == 3) {
			direction = 5 - direction;
		} else { // 1 or 2
			direction = direction + 1 + i + (ghost.id%2/* a bit better with*/)*0 - direction % 2;
		}

		direction = direction & mask;
		getNewCoordinatees(newX, newY, direction);

		canMove = checkMove(newX, newY);
		if ((canMove = canMove && !isGhost(newX, newY))) {
			ghost.lastDirection = direction;
		}
	}

	if (!canMove) {
		newPos.x = ghost.x;
		newPos.y = ghost.y;
	}
}

bool GameData::searchWithBFS(GhostFollow& ghost) {
	ghost.leftPathIndex = 0;
	Pacman& p = getPacman();
	return bfs(ghost.x, ghost.y, p.x, p.y, ghost.path, ghost.rightPathIndex);
}

void GameData::moveBots(void) {
	const char* list = creatures.CreaturesList;
	const unsigned short& listLength = creatures.CreaturesListLength;
	Point newPos;
	for (unsigned short i = 1; !justLostLife && i < listLength;  i++) {
		Ghost& ghost = (Ghost&)creatures.getCreature(list[i]);
		bool findMoves = !ghost.followPath;

		if (ghost.followPath) {
			GhostFollow& ghostWithPath = (GhostFollow&)ghost;
			Pacman& p = getPacman();

			//printf("\nFollow (%02d/%02d) (%02d, %02d) (%02d, %02d) -> [p](%02d, %02d)", ghostWithPath.leftPathIndex, ghostWithPath.rightPathIndex, ghostWithPath.x, ghostWithPath.y, ghostWithPath.path[ghostWithPath.rightPathIndex-1].x, ghostWithPath.path[ghostWithPath.rightPathIndex - 1].y, p.x, p.y);

			if (p.currentIteration > 7) { // if pacman has moved more than 7 times should research bfs
				p.currentIteration = 0;
				ghostWithPath.leftPathIndex = ghostWithPath.rightPathIndex;
			}

			if (ghostWithPath.leftPathIndex >= ghostWithPath.rightPathIndex - 1) { // research
				findMoves = !searchWithBFS(ghostWithPath);
			}
			if (!findMoves) {
				newPos = ghostWithPath.path[++ghostWithPath.leftPathIndex];

				if (isGhost(newPos.x, newPos.y)) {
					findMoves = true;
					ghostWithPath.leftPathIndex = ghostWithPath.rightPathIndex;
				}
			}
		}

		if (findMoves) {
			findMoveBots(ghost, newPos);
		}

		move(ghost, newPos.x, newPos.y, true);
	}
	justLostLife = false;
}

void GameData::handleEvents(const sf::Event event) {
	if (event.type == sf::Event::KeyPressed && 71 <= event.key.code && event.key.code <= 74) { // mooving

		Pacman& pacman = getPacman();
		unsigned short newX = pacman.x;
		unsigned short newY = pacman.y;
		//printf("\npos(%02d, %02d)[%c]", newX, newY, map[newY][newX]);

		unsigned short direction = event.key.code - 71;
		getNewCoordinatees(newX, newY, direction);

		if (checkMove(newX, newY)) {
			//printf("\nCan move");
			const char& newCell = map[newY][newX];
			if (newCell == '.') {
				pacman.points++;
				//printf("\tpoints(%d)", pacman.points);
				if (pacman.points == 23) {
					for (int i = numberOfFences - 1; i >= 0; i--) {
						map[fences[i].y][fences[i].x] = ' ';
					}
					numberOfFences = 0;
				} else if (pacman.points == 240) {
					gameWindow.printLoseMessage('w');
					stopGame();
				}
			}
			move(pacman, newX, newY, false);

		} else {
			//printf("\nCannot move");
		}

	}
}

const unsigned short& GameData::getPointsReference(void) {
	return getPacman().points;
}

const unsigned short& GameData::getLifesReference(void) {
	return getPacman().remainingLifes;
}
