#include "config.h"
#include <string.h>

void Config::copyDefaultLevelPrefix(void) {
	memcpy(levelPrefixArr, DEFAULT_LEVEL_FILENAME_PREFIX, strlen(DEFAULT_LEVEL_FILENAME_PREFIX) + 1);
}

Config::Config() 
	: startLevelSelect(DEFAULT_START_LEVEL_SELECT)
{
	copyDefaultLevelPrefix();
}

Config::Config(const bool _startLevelSelect, const char* const _levelFilenameTemplate)
	: startLevelSelect(_startLevelSelect)
{
	unsigned int _levelFilenameTemplateLen = strlen(_levelFilenameTemplate);
	if (5 < _levelFilenameTemplateLen && _levelFilenameTemplateLen < MAX_LEVEL_LENGTH-1) {
		memcpy(levelPrefixArr, _levelFilenameTemplate, _levelFilenameTemplateLen + 1);
	} else {
		copyDefaultLevelPrefix();
	}
}