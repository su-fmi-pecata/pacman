#ifndef PARSEFILE_H_INCLUDED
#define PARSEFILE_H_INCLUDED

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"


class ParseFile {
	private:
		char readBuffer[65536];

	public:
		ParseFile(void);
		rapidjson::Document readFile(const char*);
};

#endif // PARSEFILE_H_INCLUDED
