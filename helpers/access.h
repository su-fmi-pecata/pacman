#ifndef ACCESS_H_INCLUDED
#define ACCESS_H_INCLUDED

#include "config.h"
#include "window.h"
#include "parseFile.h"
#include "gameData.h"


class Access {
public:
	ParseFile& parseFile;
	Config& config;
	GameWindow& gameWindow;
	GameData& gameData;

	Access(ParseFile& pf, Config& conf, GameWindow& gw, GameData& gd)
		: parseFile(pf),
		config(conf),
		gameWindow(gw),
		gameData(gd)
	{};
};


#endif // ACCESS_H_INCLUDED
