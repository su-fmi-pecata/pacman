#ifndef HELPER_H_INCLUDED
#define HELPER_H_INCLUDED

class Helper {
	public:
		static unsigned short min(const unsigned short&, const unsigned short&);
		static unsigned short max(const unsigned short&, const unsigned short&);
		static unsigned int strlen(const char*);
		static void concatenate(const char*, char*);
};

#endif // HELPER_H_INCLUDED
