#include "helpers/access.h"
#include "helpers/init.h"
#include "helpers/window.h"
#include <SFML/Graphics.hpp>
#include <time.h>

int main() {
	Init init;
	init.readConfig();
	init.readLevel();
	Access access(init.parseFile, *init.config, *init.gameWindow, *init.gameData);
	sf::Event event;

	clock_t lastMovedBotsTime = -access.config.waitForBotMoveTime + 100;
	clock_t now = 0;

	//access.gameWindow.init();
	const unsigned short& points = access.gameData.getPointsReference();
	const unsigned short& lifes = access.gameData.getLifesReference();
	bool isPlaying;
	while (true) {
		isPlaying = access.gameData.isPlaying();
		now = clock();

		access.gameWindow.draw(access.gameData.getMaze(), lifes, points);
		if (access.gameWindow.hasEventToHandle(event)) {
			if (event.type == sf::Event::Closed) {
				access.gameData.stopGame();
				break;
			} else if(isPlaying) {
				access.gameData.handleEvents(event);
			}
		}

		if (isPlaying && lastMovedBotsTime + access.config.waitForBotMoveTime < now) {
			access.gameData.moveBots();
			lastMovedBotsTime = now;
		}
	}


    return 0;
}
