@echo off

SET F="obj\"
IF EXIST %F% RMDIR /S /Q %F%
mkdir %F%
mkdir %F%Debug\
mkdir %F%Debug\helpers\

SET F="bin\"
IF EXIST %F% RMDIR /S /Q %F%
mkdir %F%
SET F=%F%Debug\
mkdir %F%
mkdir %F%data\

xcopy /E /Y "SFML-2.5.1\bin" %F%
REM if you want to strat directly from .exe
REM u have to include this line
xcopy /E /Y "data" %F%"data"
@echo on

g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\helpers\config.cpp" -o obj\Debug\helpers\config.o
g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\helpers\creaturesClass.cpp" -o obj\Debug\helpers\creaturesClass.o
g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\helpers\window.cpp" -o obj\Debug\helpers\window.o
g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\helpers\gameData.cpp" -o obj\Debug\helpers\gameData.o
g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\helpers\helper.cpp" -o obj\Debug\helpers\helper.o
g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\helpers\init.cpp" -o obj\Debug\helpers\init.o
g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\helpers\parseFile.cpp" -o obj\Debug\helpers\parseFile.o
g++ -Wall -w -std=c++11 -Weffc++ -Wextra -g -ISFML-2.5.1\include -Irapidjson-master\include\ -c "D:\Petar\Seafile\My Library\C++\Pacman\Main.cpp" -o obj\Debug\Main.o
g++ -LSFML-2.5.1\lib -o bin\Debug\Pacman.exe obj\Debug\helpers\config.o obj\Debug\helpers\creaturesClass.o obj\Debug\helpers\gameData.o obj\Debug\helpers\helper.o obj\Debug\helpers\init.o obj\Debug\helpers\parseFile.o obj\Debug\helpers\window.o obj\Debug\Main.o   SFML-2.5.1\lib\libsfml-audio-d.a SFML-2.5.1\lib\libsfml-graphics-d.a SFML-2.5.1\lib\libsfml-system-d.a SFML-2.5.1\lib\libsfml-window-d.a

@echo off
IF %0 == "%~0"  pause
